import { ALL_COMICS } from "../config/endpoints/Endpoints";
const { REACT_APP_PUBLIC, REACT_APP_HASH } = process.env;

export const getComicList = (page: number) => (dispatch: any) => {
  try {
    dispatch({
      type: "COMIC_LIST_LOADING",
    });

    const perPage: number = 12;
    const offset: number = page * perPage - perPage;

    fetch(
      ALL_COMICS +
        `?ts=1&apikey=${REACT_APP_PUBLIC}&hash=${REACT_APP_HASH}&limit=${perPage}&offset=${offset}`
    )
      .then((response) => response.json())
      .then((data) => {
        dispatch({
          type: "COMIC_LIST_SUCCESS",
          payload: data,
        });
      });
  } catch (e) {
    dispatch({
      type: "COMIC_LIST_FAIL",
    });
  }
};

export const getComicListByTitle = (name: string) => (dispatch: any) => {
  try {
    dispatch({
      type: "COMIC_LIST_LOADING",
    });

    fetch(
      ALL_COMICS +
        `?ts=1&apikey=${REACT_APP_PUBLIC}&hash=${REACT_APP_HASH}&title=${name}`
    )
      .then((response) => response.json())
      .then((data) => {
        dispatch({
          type: "COMIC_LIST_SUCCESS",
          payload: data,
        });
      });
  } catch (e) {
    dispatch({
      type: "COMIC_LIST_FAIL",
    });
  }
};

export const getComicsByFormat = (name: string) => (dispatch: any) => {
  try {
    dispatch({
      type: "COMIC_LIST_LOADING",
    });

    fetch(
      ALL_COMICS +
        `?ts=1&apikey=${REACT_APP_PUBLIC}&hash=${REACT_APP_HASH}&format=${name}`
    )
      .then((response) => response.json())
      .then((data) => {
        dispatch({
          type: "COMIC_LIST_SUCCESS",
          payload: data,
        });
      });
  } catch (e) {
    dispatch({
      type: "COMIC_LIST_FAIL",
    });
  }
};

export const getComicCharacters = (name: string) => (dispatch: any) => {
  try {
    dispatch({
      type: "COMIC_LIST_LOADING",
    });

    fetch(
      ALL_COMICS +
        `?ts=1&apikey=${REACT_APP_PUBLIC}&hash=${REACT_APP_HASH}&title=${name}`
    )
      .then((response) => response.json())
      .then((data) => {
        fetch(
          ALL_COMICS +
            `/${data.data.results[0].id}/characters?ts=1&apikey=${REACT_APP_PUBLIC}&hash=${REACT_APP_HASH}`
        )
          .then((res) => res.json())
          .then((characters) => {
            console.log(characters);
            dispatch({
              type: "COMIC_LIST_SUCCESS",
              payload: characters,
            });
          });
      });
  } catch (e) {
    dispatch({
      type: "COMIC_LIST_FAIL",
    });
  }
};

export const getComicStories = (name: string) => (dispatch: any) => {
  try {
    dispatch({
      type: "COMIC_LIST_LOADING",
    });

    fetch(
      ALL_COMICS +
        `?ts=1&apikey=${REACT_APP_PUBLIC}&hash=${REACT_APP_HASH}&title=${name}`
    )
      .then((response) => response.json())
      .then((data) => {
          console.log(data);
        fetch(
          ALL_COMICS +
            `/${data.data.results[0].id}/stories?ts=1&apikey=${REACT_APP_PUBLIC}&hash=${REACT_APP_HASH}`
        )
          .then((res) => res.json())
          .then((characters) => {
            console.log(characters);
            dispatch({
              type: "COMIC_LIST_SUCCESS",
              payload: characters,
            });
          });
      });
  } catch (e) {
    dispatch({
      type: "COMIC_LIST_FAIL",
    });
  }
};

export const getComic = (id: number) => (dispatch: any) => {
    try {
      dispatch({
        type: "COMIC_MULTIPLE_LOADING",
      });
  
      fetch(
        ALL_COMICS +
          `/${id}?ts=1&apikey=${REACT_APP_PUBLIC}&hash=${REACT_APP_HASH}`
      )
        .then((response) => response.json())
        .then((data) => {
          dispatch({
            type: "COMIC_MULTIPLE_SUCCESS",
            payload: data,
            comic: data.data.results[0].id
          });
        });
    } catch (e) {
      dispatch({
        type: "COMIC_MULTIPLE_FAIL",
      });
    }
  }