import { ALL_CHARACTERS, ALL_COMICS, ALL_STORIES } from "../config/endpoints/Endpoints";
const { REACT_APP_PUBLIC, REACT_APP_HASH } = process.env;

//Fetches the array of characters for a single page
export const getCharacterList = (page: number) => (dispatch: any) => {
  try {
    dispatch({
      type: "CHARACTER_LIST_LOADING",
    });

    const perPage: number = 12;
    const offset: number = (page * perPage) - perPage;

    fetch(
      ALL_CHARACTERS +
        `?ts=1&apikey=${REACT_APP_PUBLIC}&hash=${REACT_APP_HASH}&limit=${perPage}&offset=${offset}`
    )
      .then((response) => response.json())
      .then((data) => {
        dispatch({
          type: "CHARACTER_LIST_SUCCESS",
          payload: data,
        });
      });
  } catch (e) {
    dispatch({
      type: "CHARACTER_LIST_FAIL",
    });
  }
};

//Fetches an array of comics where a character with 'name' appears
export const getCharacterComics = (name: string) => (dispatch: any) => {
  try {
    dispatch({
      type: "CHARACTER_LIST_LOADING",
    });

    fetch(
      ALL_CHARACTERS +
        `?ts=1&apikey=${REACT_APP_PUBLIC}&hash=${REACT_APP_HASH}&name=${name}`
    )
      .then((response) => response.json())
      .then((data) => {
        console.log(data.data.results[0].id);

        fetch(
          ALL_CHARACTERS +
          `/${data.data.results[0].id}/comics?ts=1&apikey=${REACT_APP_PUBLIC}&hash=${REACT_APP_HASH}`
        )
        .then((response) => response.json())
        .then((data) => {
          console.log(data);

          dispatch({
            type: "CHARACTER_LIST_SUCCESS",
            payload: data,
          });
        })
      });
  } catch (e) {
    dispatch({
      type: "CHARACTER_LIST_FAIL",
    });
  }
}

export const getCharacterStories = (name: string) => (dispatch: any) => {
  try {
    dispatch({
      type: "CHARACTER_LIST_LOADING",
    });

    fetch(
      ALL_CHARACTERS +
        `?ts=1&apikey=${REACT_APP_PUBLIC}&hash=${REACT_APP_HASH}&name=${name}`
    )
      .then((response) => response.json())
      .then((charData) => {
        console.log(charData.data.results[0].id);

        fetch(
          ALL_CHARACTERS +
          `/${charData.data.results[0].id}/stories?ts=1&apikey=${REACT_APP_PUBLIC}&hash=${REACT_APP_HASH}`
        )
        .then((response) => response.json())
        .then((data) => {
          console.log(data);

          dispatch({
            type: "CHARACTER_LIST_SUCCESS",
            payload: data,
          });
        })
      });
  } catch (e) {
    dispatch({
      type: "CHARACTER_LIST_FAIL",
    });
  }
}

//Fetches an array of characters by using a comic's name
export const getCharactersByComic = (name: string) => (dispatch: any) => {
  try {
    dispatch({
      type: "CHARACTER_LIST_LOADING",
    });

    fetch(
      ALL_COMICS +
        `?ts=1&apikey=${REACT_APP_PUBLIC}&hash=${REACT_APP_HASH}&title=${name}`
    )
      .then((response) => response.json())
      .then((data) => {
        console.log(data.data.results[0].id);

        fetch(
          ALL_COMICS +
          `/${data.data.results[0].id}/characters?ts=1&apikey=${REACT_APP_PUBLIC}&hash=${REACT_APP_HASH}`
        )
        .then((res) => res.json())
        .then((characters) => {
          console.log(characters);
          dispatch({
            type: "CHARACTER_LIST_SUCCESS",
            payload: characters,
          });
        })
      });
  } catch (e) {
    dispatch({
      type: "CHARACTER_LIST_FAIL",
    });
  }
}

export const getCharactersByStory = (name: string) => (dispatch: any) => {
  try {
    dispatch({
      type: "CHARACTER_LIST_LOADING",
    });

    fetch(
      ALL_STORIES +
        `?ts=1&apikey=${REACT_APP_PUBLIC}&hash=${REACT_APP_HASH}&title=${name}`
    )
      .then((response) => response.json())
      .then((data) => {
        console.log(data.data.results[0].id);

        fetch(
          ALL_COMICS +
          `/${data.data.results[0].id}/characters?ts=1&apikey=${REACT_APP_PUBLIC}&hash=${REACT_APP_HASH}`
        )
        .then((res) => res.json())
        .then((characters) => {
          console.log(characters);
          dispatch({
            type: "CHARACTER_LIST_SUCCESS",
            payload: characters,
          });
        })
      });
  } catch (e) {
    dispatch({
      type: "CHARACTER_LIST_FAIL",
    });
  }
}

//Fetches an array of Characters using its name
export const getCharacterListByName = (name: string) => (dispatch: any) => {
  try {
    dispatch({
      type: "CHARACTER_LIST_LOADING",
    });

    fetch(
      ALL_CHARACTERS +
        `?ts=1&apikey=${REACT_APP_PUBLIC}&hash=${REACT_APP_HASH}&nameStartsWith=${name}`
    )
      .then((response) => response.json())
      .then((data) => {
        dispatch({
          type: "CHARACTER_LIST_SUCCESS",
          payload: data,
        });
      });
  } catch (e) {
    dispatch({
      type: "CHARACTER_LIST_FAIL",
    });
  }
}

//Fetches the data for a single character using its id
export const getCharacter = (id: number) => (dispatch: any) => {
  try {
    dispatch({
      type: "CHARACTER_MULTIPLE_LOADING",
    });

    fetch(
      ALL_CHARACTERS +
        `/${id}?ts=1&apikey=${REACT_APP_PUBLIC}&hash=${REACT_APP_HASH}`
    )
      .then((response) => response.json())
      .then((data) => {
        dispatch({
          type: "CHARACTER_MULTIPLE_SUCCESS",
          payload: data,
          character: data.data.results[0].id
        });
      });
  } catch (e) {
    dispatch({
      type: "CHARACTER_MULTIPLE_FAIL",
    });
  }
}


