import _ from "lodash";

export const checkBookmark = (data: any, bookmarks: any) => (dispatch: any) => {
  try {
    dispatch({
      type: "BOOKMARK_LOADING",
    });

    bookmarks.forEach((item: any, index: number) => {
      console.log(_.isEqual(item, data));
      if (_.isEqual(item, data)) {
        console.log("matched, removed bookmark");
        bookmarks.splice(index, 1);
        console.log(bookmarks);
        dispatch({
          type: "BOOKMARK_SUCCESS",
          payload: bookmarks,
        });
      } else {
        if (item.id) {
          console.log("didnt match, bookmarked");
          const bookmarksCopy = [...bookmarks];
          bookmarksCopy.push(data);
          dispatch({
            type: "BOOKMARK_SUCCESS",
            payload: bookmarksCopy,
          });
        }
      }
    });
  } catch (e) {
    dispatch({
      type: "BOOKMARK_FAIL",
    });
  }
};
