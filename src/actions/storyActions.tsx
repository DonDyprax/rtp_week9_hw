import { ALL_STORIES } from "../config/endpoints/Endpoints";
const { REACT_APP_PUBLIC, REACT_APP_HASH } = process.env;

export const getStoryList = (page: number) => (dispatch: any) => {
  try {
    dispatch({
      type: "STORY_LIST_LOADING",
    });

    const perPage: number = 12;
    const offset: number = page * perPage - perPage;

    fetch(
      ALL_STORIES +
        `?ts=1&apikey=${REACT_APP_PUBLIC}&hash=${REACT_APP_HASH}&limit=${perPage}&offset=${offset}`
    )
      .then((response) => response.json())
      .then((data) => {
        dispatch({
          type: "STORY_LIST_SUCCESS",
          payload: data,
        });
      });
  } catch (e) {
    dispatch({
      type: "STORY_LIST_FAIL",
    });
  }
};

export const getStoryCharacters = (name: string) => (dispatch: any) => {
  try {
    dispatch({
      type: "STORY_LIST_LOADING",
    });

    fetch(
      ALL_STORIES + `?ts=1&apikey=${REACT_APP_PUBLIC}&hash=${REACT_APP_HASH}`
    )
      .then((response) => response.json())
      .then((data) => {
        console.log(data);
      });
  } catch (e) {
    dispatch({
      type: "STORY_LIST_FAIL",
    });
  }
};

export const getStoryComics = (name: string) => (dispatch: any) => {};

export const getStory = (id: number) => (dispatch: any) => {
  try {
    dispatch({
      type: "STORY_MULTIPLE_LOADING",
    });

    fetch(
      ALL_STORIES +
        `/${id}?ts=1&apikey=${REACT_APP_PUBLIC}&hash=${REACT_APP_HASH}`
    )
      .then((response) => response.json())
      .then((data) => {
          console.log(data);
        dispatch({
          type: "STORY_MULTIPLE_SUCCESS",
          payload: data,
          story: data.data.results[0].id,
        });
      });
  } catch (e) {
    dispatch({
      type: "STORY_MULTIPLE_FAIL",
    });
  }
};
