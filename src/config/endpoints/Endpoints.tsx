export const ALL_CHARACTERS = 'https://gateway.marvel.com/v1/public/characters';
export const ALL_COMICS = 'https://gateway.marvel.com/v1/public/comics';
export const ALL_STORIES = 'https://gateway.marvel.com/v1/public/stories';