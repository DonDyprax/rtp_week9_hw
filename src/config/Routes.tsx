import React from "react";
import Navbar from "../components/common/Navbar";
import Home from "../pages/Home";
import Characters from "../pages/Characters";
import Character from "../pages/Character";
import Comics from "../pages/Comics";
import Comic from "../pages/Comic";
import Stories from "../pages/Stories";
import Story from "../pages/Story";
import { BrowserRouter as Router, Switch, Route, Redirect } from "react-router-dom";

const Routes = () => {
  return (
    <Router>
      <div className="app">
        <Navbar />
        <Switch>
          <Route path="/" exact component={Home} />
          <Route path="/characters" exact component={Characters} />
          <Route path="/comics" exact component={Comics} />
          <Route path="/stories" exact component={Stories} />
          <Route path="/characters/:id" exact component={Character}/>
          <Route path="/comics/:id" exact component={Comic}/>
          <Route path="/stories/:id" exact component={Story} />
          <Redirect to="/" />
        </Switch>
      </div>
    </Router>
  );
};

export default Routes;
