
export default interface Story {
    id: number;
    title: string;
    description: string;
    resourceURI: string;
    type: string;
    modified: string;
    thumbnail: any;
    creators: {available: number, collectionURI: string, items: Array<{resourceURI: string, name: string, role: string}>, returned: number};
    characters: {available: number, collectionURI: string, items: Array<{resourceURI: string, name: string}>, returned: number};
    series: {available: number, collectionURI: string, items: Array<{resourceURI: string, name: string}>, returned: number};
    comics:  {available: number, collectionURI: string, items: Array<{resourceURI: string, name: string}>, returned: number};
    events: {available: number, colecctionURI: string, items: Array<any>, returned: number};
    originalIssue: {resourceURI: string, name: string};
}