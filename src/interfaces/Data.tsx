export default interface Data<T> {
    offset: number;
    limit: number;
    total: number;
    count: number;
    results: Array<T>;
}