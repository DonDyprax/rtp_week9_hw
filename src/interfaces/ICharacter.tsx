import Character from "./Character";
import Request from "./Request";
import Data from "./Data";

export default interface ICharacter extends Request, Data<Character> {

}