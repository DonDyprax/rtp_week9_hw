import { rest } from 'msw';
import { characterListResponse } from './responses/characterListResponse';
import { comicListResponse } from './responses/comicListResponse';
import { characterSearchResponse } from './responses/characterSearchResponse';
import { screen } from '@testing-library/react';
import { storyListResponse } from './responses/storyListResponse';
import { characterResponse } from './responses/characterResponse';
import { comicResponse } from './responses/comicResponse';
import { storyResponse } from './responses/storyResponse';
import { characterComicResponse } from './responses/characterComicResponse';


export const handlers = [
    rest.get('https://gateway.marvel.com/v1/public/characters', (req, res, ctx) => {
        const inputValue = (screen.getByTestId('searchInput') as HTMLInputElement).value;
        if(inputValue !== '') {
            if(inputValue === '3-D Man') {
                return res(ctx.status(200), ctx.json(characterComicResponse));
            }
            
            return res(ctx.status(200), ctx.json(characterSearchResponse));
        }

        return res(ctx.status(200), ctx.json(characterListResponse));
    }),
    rest.get('https://gateway.marvel.com/v1/public/characters/1011334', (req, res, ctx) => {
        return res(ctx.status(200), ctx.json(characterResponse));        
    }),
    rest.get('https://gateway.marvel.com/v1/public/comics', (req, res, ctx) => {
        return res(ctx.status(200), ctx.json(comicListResponse));        
    }),
    rest.get('https://gateway.marvel.com/v1/public/comics/6181', (req, res, ctx) => {
        return res(ctx.status(200), ctx.json(comicResponse));        
    }),
    rest.get('https://gateway.marvel.com/v1/public/stories', (req, res, ctx) => {
        return res(ctx.status(200), ctx.json(storyListResponse));
    }),
    rest.get('https://gateway.marvel.com/v1/public/stories/659', (req, res, ctx) => {
        return res(ctx.status(200), ctx.json(storyResponse));
    })
]