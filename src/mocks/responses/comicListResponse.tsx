export const comicListResponse = {
  code: 200,
  status: "Ok",
  copyright: "© 2021 MARVEL",
  attributionText: "Data provided by Marvel. © 2021 MARVEL",
  attributionHTML:
    '<a href="http://marvel.com">Data provided by Marvel. © 2021 MARVEL</a>',
  etag: "c65a3f56b8613b555484dcceedd3b17d3483c93c",
  data: {
    offset: 28,
    limit: 3,
    total: 48585,
    count: 3,
    results: [
      {
        id: 6181,
        digitalId: 0,
        title:
          "Ultimate Spider-Man Ultimate Collection Book 1 (Trade Paperback)",
        issueNumber: 0,
        variantDescription: "",
        description:
          "Collecting the groundbreaking first year of Ultimate Spider-Man in one colossal trade paperback! High school, puberty, first dances - there are many pitfalls to being young. Compound these with intense personal tragedy and super human powers, and you can start to visualize the world of Peter Parker, a.k.a. Spider-Man. Following the murder of his uncle and the Green Goblin's assault on his high school, Peter finds himself on the brink of manhood: getting a job at the Daily Bugle to help support his widowed aunt and taking on extracurricular activities - such as bringing down the Kingpin, the head of organized crime in New York City! Collecting ULTIMATE SPIDER-MAN #1-13.\r\n352 PGS./Rated A ...$24.99",
        modified: "2020-08-25T09:58:06-0400",
        isbn: "0-7851-2492-6",
        upc: "5960612492-00111",
        diamondCode: "",
        ean: "",
        issn: "",
        format: "Trade Paperback",
        pageCount: 0,
        textObjects: [
          {
            type: "issue_solicit_text",
            language: "en-us",
            text:
              "Collecting the groundbreaking first year of Ultimate Spider-Man in one colossal trade paperback! High school, puberty, first dances - there are many pitfalls to being young. Compound these with intense personal tragedy and super human powers, and you can start to visualize the world of Peter Parker, a.k.a. Spider-Man. Following the murder of his uncle and the Green Goblin's assault on his high school, Peter finds himself on the brink of manhood: getting a job at the Daily Bugle to help support his widowed aunt and taking on extracurricular activities - such as bringing down the Kingpin, the head of organized crime in New York City! Collecting ULTIMATE SPIDER-MAN #1-13.\r\n352 PGS./Rated A ...$24.99",
          },
        ],
        resourceURI: "http://gateway.marvel.com/v1/public/comics/6181",
        urls: [
          {
            type: "detail",
            url:
              "http://marvel.com/comics/collection/6181/ultimate_spider-man_ultimate_collection_book_1_trade_paperback?utm_campaign=apiRef&utm_source=0973dd9cba7affef8dbe4c0dfc2175df",
          },
        ],
        series: {
          resourceURI: "http://gateway.marvel.com/v1/public/series/1919",
          name: "Ultimate Spider-Man Ultimate Collection Book 1 (2007)",
        },
        variants: [],
        collections: [],
        collectedIssues: [
          {
            resourceURI: "http://gateway.marvel.com/v1/public/comics/14838",
            name: "Ultimate Spider-Man (2000) #12",
          },
          {
            resourceURI: "http://gateway.marvel.com/v1/public/comics/14837",
            name: "Ultimate Spider-Man (2000) #11",
          },
          {
            resourceURI: "http://gateway.marvel.com/v1/public/comics/14836",
            name: "Ultimate Spider-Man (2000) #10",
          },
          {
            resourceURI: "http://gateway.marvel.com/v1/public/comics/14890",
            name: "Ultimate Spider-Man (2000) #6",
          },
          {
            resourceURI: "http://gateway.marvel.com/v1/public/comics/14912",
            name: "Ultimate Spider-Man (2000) #8",
          },
          {
            resourceURI: "http://gateway.marvel.com/v1/public/comics/14901",
            name: "Ultimate Spider-Man (2000) #7",
          },
          {
            resourceURI: "http://gateway.marvel.com/v1/public/comics/14879",
            name: "Ultimate Spider-Man (2000) #5",
          },
          {
            resourceURI: "http://gateway.marvel.com/v1/public/comics/14868",
            name: "Ultimate Spider-Man (2000) #4",
          },
          {
            resourceURI: "http://gateway.marvel.com/v1/public/comics/14857",
            name: "Ultimate Spider-Man (2000) #3",
          },
          {
            resourceURI: "http://gateway.marvel.com/v1/public/comics/14846",
            name: "Ultimate Spider-Man (2000) #2",
          },
          {
            resourceURI: "http://gateway.marvel.com/v1/public/comics/4372",
            name: "Ultimate Spider-Man (2000) #1",
          },
        ],
        dates: [
          {
            type: "onsaleDate",
            date: "2029-12-31T00:00:00-0500",
          },
          {
            type: "focDate",
            date: "1961-01-01T00:00:00-0500",
          },
        ],
        prices: [
          {
            type: "printPrice",
            price: 9.99,
          },
        ],
        thumbnail: {
          path: "http://i.annihil.us/u/prod/marvel/i/mg/6/c0/59079911f0fdb",
          extension: "jpg",
        },
        images: [
          {
            path: "http://i.annihil.us/u/prod/marvel/i/mg/6/c0/59079911f0fdb",
            extension: "jpg",
          },
          {
            path: "http://i.annihil.us/u/prod/marvel/i/mg/9/70/4bc5a059dc189",
            extension: "jpg",
          },
        ],
        creators: {
          available: 10,
          collectionURI:
            "http://gateway.marvel.com/v1/public/comics/6181/creators",
          items: [
            {
              resourceURI: "http://gateway.marvel.com/v1/public/creators/4736",
              name: "Jc",
              role: "colorist",
            },
            {
              resourceURI: "http://gateway.marvel.com/v1/public/creators/1909",
              name: "Steve Buccellato",
              role: "colorist",
            },
            {
              resourceURI: "http://gateway.marvel.com/v1/public/creators/6694",
              name: "Wes Abbot",
              role: "letterer",
            },
            {
              resourceURI: "http://gateway.marvel.com/v1/public/creators/3603",
              name: "Albert Deschesne",
              role: "letterer",
            },
            {
              resourceURI: "http://gateway.marvel.com/v1/public/creators/350",
              name: "Richard Starkings",
              role: "letterer",
            },
            {
              resourceURI: "http://gateway.marvel.com/v1/public/creators/87",
              name: "Mark Bagley",
              role: "penciler",
            },
            {
              resourceURI: "http://gateway.marvel.com/v1/public/creators/24",
              name: "Brian Michael Bendis",
              role: "writer",
            },
            {
              resourceURI: "http://gateway.marvel.com/v1/public/creators/210",
              name: "Bill Jemas",
              role: "writer",
            },
            {
              resourceURI: "http://gateway.marvel.com/v1/public/creators/13152",
              name: "Joe Quesada",
              role: "editor",
            },
            {
              resourceURI: "http://gateway.marvel.com/v1/public/creators/568",
              name: "Art Thibert",
              role: "inker",
            },
          ],
          returned: 10,
        },
        characters: {
          available: 7,
          collectionURI:
            "http://gateway.marvel.com/v1/public/comics/6181/characters",
          items: [
            {
              resourceURI:
                "http://gateway.marvel.com/v1/public/characters/1014991",
              name: "Crusher Hogan (Ultimate)",
            },
            {
              resourceURI:
                "http://gateway.marvel.com/v1/public/characters/1010922",
              name: "Electro (Ultimate)",
            },
            {
              resourceURI:
                "http://gateway.marvel.com/v1/public/characters/1014974",
              name: "George Stacy (Ultimate)",
            },
            {
              resourceURI:
                "http://gateway.marvel.com/v1/public/characters/1011218",
              name: "Harry Osborn (Ultimate)",
            },
            {
              resourceURI:
                "http://gateway.marvel.com/v1/public/characters/1011203",
              name: "Mary Jane Watson (Ultimate)",
            },
            {
              resourceURI:
                "http://gateway.marvel.com/v1/public/characters/1011010",
              name: "Spider-Man (Ultimate)",
            },
            {
              resourceURI:
                "http://gateway.marvel.com/v1/public/characters/1011128",
              name: "Venom (Ultimate)",
            },
          ],
          returned: 7,
        },
        stories: {
          available: 29,
          collectionURI:
            "http://gateway.marvel.com/v1/public/comics/6181/stories",
          items: [
            {
              resourceURI: "http://gateway.marvel.com/v1/public/stories/528",
              name: "Interior #528",
              type: "interiorStory",
            },
            {
              resourceURI: "http://gateway.marvel.com/v1/public/stories/8038",
              name: "Ultimate Spider-Man 1-13",
              type: "cover",
            },
            {
              resourceURI: "http://gateway.marvel.com/v1/public/stories/8039",
              name: "Ultimate Spider-Man 1-13",
              type: "interiorStory",
            },
            {
              resourceURI: "http://gateway.marvel.com/v1/public/stories/8512",
              name: "Ultimate Spider-Man 1-13",
              type: "interiorStory",
            },
            {
              resourceURI: "http://gateway.marvel.com/v1/public/stories/30293",
              name: "",
              type: "",
            },
            {
              resourceURI: "http://gateway.marvel.com/v1/public/stories/30294",
              name: "",
              type: "",
            },
            {
              resourceURI: "http://gateway.marvel.com/v1/public/stories/30296",
              name: "Previously. . .",
              type: "recap",
            },
            {
              resourceURI: "http://gateway.marvel.com/v1/public/stories/30297",
              name: "[",
              type: "interiorStory",
            },
            {
              resourceURI: "http://gateway.marvel.com/v1/public/stories/30298",
              name: "[Spidey Letters]",
              type: "letters",
            },
            {
              resourceURI: "http://gateway.marvel.com/v1/public/stories/30326",
              name: "Previously. . .",
              type: "recap",
            },
            {
              resourceURI: "http://gateway.marvel.com/v1/public/stories/30327",
              name: "Growing Pains",
              type: "interiorStory",
            },
            {
              resourceURI: "http://gateway.marvel.com/v1/public/stories/30352",
              name: "Previously . . .",
              type: "recap",
            },
            {
              resourceURI: "http://gateway.marvel.com/v1/public/stories/30353",
              name: "Wannabe",
              type: "interiorStory",
            },
            {
              resourceURI: "http://gateway.marvel.com/v1/public/stories/30354",
              name: "[Spidey Letters]",
              type: "letters",
            },
            {
              resourceURI: "http://gateway.marvel.com/v1/public/stories/30366",
              name: "Previously . . .",
              type: "recap",
            },
            {
              resourceURI: "http://gateway.marvel.com/v1/public/stories/30367",
              name: "With Great Power",
              type: "interiorStory",
            },
            {
              resourceURI: "http://gateway.marvel.com/v1/public/stories/30368",
              name: "[Spidey Letters]",
              type: "letters",
            },
            {
              resourceURI: "http://gateway.marvel.com/v1/public/stories/30383",
              name: "Previously. . .",
              type: "recap",
            },
            {
              resourceURI: "http://gateway.marvel.com/v1/public/stories/30384",
              name: "Life Lessons",
              type: "interiorStory",
            },
            {
              resourceURI: "http://gateway.marvel.com/v1/public/stories/30385",
              name: "[Spidey Letters]",
              type: "letters",
            },
          ],
          returned: 20,
        },
        events: {
          available: 0,
          collectionURI:
            "http://gateway.marvel.com/v1/public/comics/6181/events",
          items: [],
          returned: 0,
        },
      },
      {
        id: 291,
        digitalId: 0,
        title: "Ant-Man (2003) #1",
        issueNumber: 1,
        variantDescription: "",
        description:
          "Size does matter.  And no one knows this more than Hank Pym - a.k.a. Ant-Man. Got a problem with Galactus? Call the FF. Got a problem with, say, mind-controlled cockroaches? Then Ant-Man's your man! And needless to say, it's done a number on our diminutive hero's self-esteem.  When Ant-Man is tapped to infiltrate an international spy ring that has been siphoning secrets out of Washington, he jumps at the chance - unaware that he's being used as a pawn in a larger game of espionage.\r\n32 PGS./PARENTAL ADVISORY...$2.99",
        modified: "-0001-11-30T00:00:00-0500",
        isbn: "",
        upc: "5960605396-01811",
        diamondCode: "",
        ean: "",
        issn: "",
        format: "Comic",
        pageCount: 0,
        textObjects: [
          {
            type: "issue_solicit_text",
            language: "en-us",
            text:
              "Size does matter.  And no one knows this more than Hank Pym - a.k.a. Ant-Man. Got a problem with Galactus? Call the FF. Got a problem with, say, mind-controlled cockroaches? Then Ant-Man's your man! And needless to say, it's done a number on our diminutive hero's self-esteem.  When Ant-Man is tapped to infiltrate an international spy ring that has been siphoning secrets out of Washington, he jumps at the chance - unaware that he's being used as a pawn in a larger game of espionage.\r\n32 PGS./PARENTAL ADVISORY...$2.99",
          },
        ],
        resourceURI: "http://gateway.marvel.com/v1/public/comics/291",
        urls: [
          {
            type: "detail",
            url:
              "http://marvel.com/comics/issue/291/ant-man_2003_1?utm_campaign=apiRef&utm_source=0973dd9cba7affef8dbe4c0dfc2175df",
          },
        ],
        series: {
          resourceURI: "http://gateway.marvel.com/v1/public/series/551",
          name: "Ant-Man (2003 - 2004)",
        },
        variants: [],
        collections: [],
        collectedIssues: [],
        dates: [
          {
            type: "onsaleDate",
            date: "2029-12-31T00:00:00-0500",
          },
          {
            type: "focDate",
            date: "-0001-11-30T00:00:00-0500",
          },
        ],
        prices: [
          {
            type: "printPrice",
            price: 2.99,
          },
        ],
        thumbnail: {
          path: "http://i.annihil.us/u/prod/marvel/i/mg/6/e0/4bc6a2497684e",
          extension: "jpg",
        },
        images: [
          {
            path: "http://i.annihil.us/u/prod/marvel/i/mg/6/e0/4bc6a2497684e",
            extension: "jpg",
          },
        ],
        creators: {
          available: 2,
          collectionURI:
            "http://gateway.marvel.com/v1/public/comics/291/creators",
          items: [
            {
              resourceURI: "http://gateway.marvel.com/v1/public/creators/600",
              name: "Clayton Crain",
              role: "penciller (cover)",
            },
            {
              resourceURI: "http://gateway.marvel.com/v1/public/creators/344",
              name: "Daniel Way",
              role: "writer",
            },
          ],
          returned: 2,
        },
        characters: {
          available: 0,
          collectionURI:
            "http://gateway.marvel.com/v1/public/comics/291/characters",
          items: [],
          returned: 0,
        },
        stories: {
          available: 2,
          collectionURI:
            "http://gateway.marvel.com/v1/public/comics/291/stories",
          items: [
            {
              resourceURI: "http://gateway.marvel.com/v1/public/stories/1806",
              name: "Cover #1806",
              type: "cover",
            },
            {
              resourceURI: "http://gateway.marvel.com/v1/public/stories/1807",
              name: "Interior #1807",
              type: "interiorStory",
            },
          ],
          returned: 2,
        },
        events: {
          available: 0,
          collectionURI:
            "http://gateway.marvel.com/v1/public/comics/291/events",
          items: [],
          returned: 0,
        },
      },
      {
        id: 1158,
        digitalId: 0,
        title: "ULTIMATE X-MEN VOL. 5: ULTIMATE WAR TPB (Trade Paperback)",
        issueNumber: 0,
        variantDescription: "",
        description:
          "The Ultimates vs. the Ultimate X-Men: the battle begins. When the X-Men do the worst thing they could to humanity, the government orders Captain America, Iron Man, Thor and the rest of the Ultimates to bring them down. A small but lethal army, the Ultimates were created to face these and other newly rising threats to mankind. But the X-Men's founder, Professor X, hasn't been training his students for nothing -- and the youngs mutants just might take out the Ultimates first.",
        modified: "2018-03-12T16:56:16-0400",
        isbn: "0-7851-1129-8",
        upc: "",
        diamondCode: "",
        ean: "",
        issn: "",
        format: "Trade Paperback",
        pageCount: 112,
        textObjects: [
          {
            type: "issue_solicit_text",
            language: "en-us",
            text:
              "The Ultimates vs. the Ultimate X-Men: the battle begins. When the X-Men do the worst thing they could to humanity, the government orders Captain America, Iron Man, Thor and the rest of the Ultimates to bring them down. A small but lethal army, the Ultimates were created to face these and other newly rising threats to mankind. But the X-Men's founder, Professor X, hasn't been training his students for nothing -- and the youngs mutants just might take out the Ultimates first.",
          },
        ],
        resourceURI: "http://gateway.marvel.com/v1/public/comics/1158",
        urls: [
          {
            type: "detail",
            url:
              "http://marvel.com/comics/collection/1158/ultimate_x-men_vol_5_ultimate_war_tpb_trade_paperback?utm_campaign=apiRef&utm_source=0973dd9cba7affef8dbe4c0dfc2175df",
          },
        ],
        series: {
          resourceURI: "http://gateway.marvel.com/v1/public/series/216",
          name: "ULTIMATE X-MEN VOL. 5: ULTIMATE WAR TPB (1999)",
        },
        variants: [],
        collections: [],
        collectedIssues: [
          {
            resourceURI: "http://gateway.marvel.com/v1/public/comics/18477",
            name: "Ultimate War (2003) #4",
          },
          {
            resourceURI: "http://gateway.marvel.com/v1/public/comics/18476",
            name: "Ultimate War (2003) #3",
          },
          {
            resourceURI: "http://gateway.marvel.com/v1/public/comics/18475",
            name: "Ultimate War (2003) #2",
          },
          {
            resourceURI: "http://gateway.marvel.com/v1/public/comics/18474",
            name: "Ultimate War (2003) #1",
          },
        ],
        dates: [
          {
            type: "onsaleDate",
            date: "2029-12-31T00:00:00-0500",
          },
          {
            type: "focDate",
            date: "1961-01-01T00:00:00-0500",
          },
        ],
        prices: [
          {
            type: "printPrice",
            price: 9.99,
          },
        ],
        thumbnail: {
          path: "http://i.annihil.us/u/prod/marvel/i/mg/2/f0/4bc6670c80007",
          extension: "jpg",
        },
        images: [
          {
            path: "http://i.annihil.us/u/prod/marvel/i/mg/2/f0/4bc6670c80007",
            extension: "jpg",
          },
        ],
        creators: {
          available: 9,
          collectionURI:
            "http://gateway.marvel.com/v1/public/comics/1158/creators",
          items: [
            {
              resourceURI: "http://gateway.marvel.com/v1/public/creators/232",
              name: "Chris Bachalo",
              role: "penciller",
            },
            {
              resourceURI: "http://gateway.marvel.com/v1/public/creators/452",
              name: "Virtual Calligr",
              role: "letterer",
            },
            {
              resourceURI: "http://gateway.marvel.com/v1/public/creators/6170",
              name: "Olivier Coipel",
              role: "penciler",
            },
            {
              resourceURI: "http://gateway.marvel.com/v1/public/creators/312",
              name: "Mike Deodato",
              role: "penciler",
            },
            {
              resourceURI: "http://gateway.marvel.com/v1/public/creators/181",
              name: "Geoff Johns",
              role: "writer",
            },
            {
              resourceURI: "http://gateway.marvel.com/v1/public/creators/52",
              name: "Bruce Jones",
              role: "writer",
            },
            {
              resourceURI: "http://gateway.marvel.com/v1/public/creators/88",
              name: "Mark Millar",
              role: "writer",
            },
            {
              resourceURI: "http://gateway.marvel.com/v1/public/creators/479",
              name: "Paul Mounts",
              role: "colorist",
            },
            {
              resourceURI: "http://gateway.marvel.com/v1/public/creators/427",
              name: "Tim Townsend",
              role: "inker",
            },
          ],
          returned: 9,
        },
        characters: {
          available: 17,
          collectionURI:
            "http://gateway.marvel.com/v1/public/comics/1158/characters",
          items: [
            {
              resourceURI:
                "http://gateway.marvel.com/v1/public/characters/1010908",
              name: "Beast (Ultimate)",
            },
            {
              resourceURI:
                "http://gateway.marvel.com/v1/public/characters/1010911",
              name: "Black Widow (Ultimate)",
            },
            {
              resourceURI:
                "http://gateway.marvel.com/v1/public/characters/1010913",
              name: "Captain America (Ultimate)",
            },
            {
              resourceURI:
                "http://gateway.marvel.com/v1/public/characters/1010917",
              name: "Colossus (Ultimate)",
            },
            {
              resourceURI:
                "http://gateway.marvel.com/v1/public/characters/1011131",
              name: "Hawkeye (Ultimate)",
            },
            {
              resourceURI:
                "http://gateway.marvel.com/v1/public/characters/1011005",
              name: "Hulk (Ultimate)",
            },
            {
              resourceURI:
                "http://gateway.marvel.com/v1/public/characters/1010933",
              name: "Iceman (Ultimate)",
            },
            {
              resourceURI:
                "http://gateway.marvel.com/v1/public/characters/1010946",
              name: "Jean Grey (Ultimate)",
            },
            {
              resourceURI:
                "http://gateway.marvel.com/v1/public/characters/1010943",
              name: "Magneto (Ultimate)",
            },
            {
              resourceURI:
                "http://gateway.marvel.com/v1/public/characters/1011007",
              name: "Nick Fury (Ultimate)",
            },
            {
              resourceURI:
                "http://gateway.marvel.com/v1/public/characters/1010963",
              name: "Quicksilver (Ultimate)",
            },
            {
              resourceURI:
                "http://gateway.marvel.com/v1/public/characters/1010966",
              name: "Rogue (Ultimate)",
            },
            {
              resourceURI:
                "http://gateway.marvel.com/v1/public/characters/1010971",
              name: "Scarlet Witch (Ultimate)",
            },
            {
              resourceURI:
                "http://gateway.marvel.com/v1/public/characters/1010978",
              name: "Storm (Ultimate)",
            },
            {
              resourceURI:
                "http://gateway.marvel.com/v1/public/characters/1011025",
              name: "Thor (Ultimate)",
            },
            {
              resourceURI:
                "http://gateway.marvel.com/v1/public/characters/1009689",
              name: "Vanisher (Ultimate)",
            },
            {
              resourceURI:
                "http://gateway.marvel.com/v1/public/characters/1010992",
              name: "Wasp (Ultimate)",
            },
          ],
          returned: 17,
        },
        stories: {
          available: 9,
          collectionURI:
            "http://gateway.marvel.com/v1/public/comics/1158/stories",
          items: [
            {
              resourceURI: "http://gateway.marvel.com/v1/public/stories/270",
              name:
                "The Ultimates vs. the Ultimate X-Men: the battle begins. When the X-Men do the worst thing they could to humanity, the governmen",
              type: "interiorStory",
            },
            {
              resourceURI: "http://gateway.marvel.com/v1/public/stories/39348",
              name: "Interior #39348",
              type: "interiorStory",
            },
            {
              resourceURI: "http://gateway.marvel.com/v1/public/stories/39349",
              name: "Free Preview of Hulk #50",
              type: "promo",
            },
            {
              resourceURI: "http://gateway.marvel.com/v1/public/stories/39351",
              name: "Interior #39351",
              type: "interiorStory",
            },
            {
              resourceURI: "http://gateway.marvel.com/v1/public/stories/39352",
              name: "Free Preview of Hulk #50",
              type: "promo",
            },
            {
              resourceURI: "http://gateway.marvel.com/v1/public/stories/39354",
              name: "Interior #39354",
              type: "interiorStory",
            },
            {
              resourceURI: "http://gateway.marvel.com/v1/public/stories/39355",
              name: "Free Preview of Avengers 65",
              type: "promo",
            },
            {
              resourceURI: "http://gateway.marvel.com/v1/public/stories/39357",
              name: "Interior #39357",
              type: "interiorStory",
            },
            {
              resourceURI: "http://gateway.marvel.com/v1/public/stories/65260",
              name: "ULTIMATE X-MEN VOL. 5: ULTIMATE WAR 0 cover",
              type: "cover",
            },
          ],
          returned: 9,
        },
        events: {
          available: 0,
          collectionURI:
            "http://gateway.marvel.com/v1/public/comics/1158/events",
          items: [],
          returned: 0,
        },
      },
    ],
  },
};
