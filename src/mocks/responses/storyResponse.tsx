export const storyResponse = {
    code: 200,
    status: "Ok",
    copyright: "© 2021 MARVEL",
    attributionText: "Data provided by Marvel. © 2021 MARVEL",
    attributionHTML:
      '<a href="http://marvel.com">Data provided by Marvel. © 2021 MARVEL</a>',
    etag: "a9ac37cd696dfef544dd069b335fba1e19a4d48d",
    data: {
      offset: 0,
      limit: 1,
      total: 1493,
      count: 1,
      results: [
        {
            id: 659,
            title: "1 of 2 - What Lorna Saw",
            description: "",
            resourceURI: "http://gateway.marvel.com/v1/public/stories/659",
            type: "story",
            modified: "1969-12-31T19:00:00-0500",
            thumbnail: null,
            creators: {
              available: 5,
              collectionURI:
                "http://gateway.marvel.com/v1/public/stories/659/creators",
              items: [
                {
                  resourceURI: "http://gateway.marvel.com/v1/public/creators/13348",
                  name: "Liquid! Color",
                  role: "colorist",
                },
                {
                  resourceURI: "http://gateway.marvel.com/v1/public/creators/11757",
                  name: "Salvador Larroca",
                  role: "penciller",
                },
                {
                  resourceURI: "http://gateway.marvel.com/v1/public/creators/13146",
                  name: "Danny Miki",
                  role: "inker",
                },
                {
                  resourceURI: "http://gateway.marvel.com/v1/public/creators/94",
                  name: "Peter Milligan",
                  role: "writer",
                },
                {
                  resourceURI: "http://gateway.marvel.com/v1/public/creators/361",
                  name: "Cory Petit",
                  role: "letterer",
                },
              ],
              returned: 5,
            },
            characters: {
              available: 1,
              collectionURI:
                "http://gateway.marvel.com/v1/public/stories/659/characters",
              items: [
                {
                  resourceURI:
                    "http://gateway.marvel.com/v1/public/characters/1009726",
                  name: "X-Men",
                },
              ],
              returned: 1,
            },
            series: {
              available: 2,
              collectionURI:
                "http://gateway.marvel.com/v1/public/stories/659/series",
              items: [
                {
                  resourceURI: "http://gateway.marvel.com/v1/public/series/1643",
                  name: "Decimation: X-Men the Day After (2006)",
                },
                {
                  resourceURI: "http://gateway.marvel.com/v1/public/series/403",
                  name: "X-Men (2004 - 2007)",
                },
              ],
              returned: 2,
            },
            comics: {
              available: 2,
              collectionURI:
                "http://gateway.marvel.com/v1/public/stories/659/comics",
              items: [
                {
                  resourceURI: "http://gateway.marvel.com/v1/public/comics/4072",
                  name: "Decimation: X-Men the Day After (Trade Paperback)",
                },
                {
                  resourceURI: "http://gateway.marvel.com/v1/public/comics/3137",
                  name: "X-Men (2004) #180",
                },
              ],
              returned: 2,
            },
            events: {
              available: 0,
              collectionURI:
                "http://gateway.marvel.com/v1/public/stories/659/events",
              items: [],
              returned: 0,
            },
            originalIssue: {
              resourceURI: "http://gateway.marvel.com/v1/public/comics/3137",
              name: "X-Men (2004) #180",
            },
          },
          {
            id: 660,
            title: "X-MEN (2004) #181",
            description: "",
            resourceURI: "http://gateway.marvel.com/v1/public/stories/660",
            type: "cover",
            modified: "2017-02-24T14:12:38-0500",
            thumbnail: null,
            creators: {
              available: 5,
              collectionURI:
                "http://gateway.marvel.com/v1/public/stories/660/creators",
              items: [
                {
                  resourceURI: "http://gateway.marvel.com/v1/public/creators/13348",
                  name: "Liquid! Color",
                  role: "colorist",
                },
                {
                  resourceURI: "http://gateway.marvel.com/v1/public/creators/11757",
                  name: "Salvador Larroca",
                  role: "penciller (cover)",
                },
                {
                  resourceURI: "http://gateway.marvel.com/v1/public/creators/13146",
                  name: "Danny Miki",
                  role: "inker",
                },
                {
                  resourceURI: "http://gateway.marvel.com/v1/public/creators/94",
                  name: "Peter Milligan",
                  role: "writer",
                },
                {
                  resourceURI: "http://gateway.marvel.com/v1/public/creators/361",
                  name: "Cory Petit",
                  role: "letterer",
                },
              ],
              returned: 5,
            },
            characters: {
              available: 7,
              collectionURI:
                "http://gateway.marvel.com/v1/public/stories/660/characters",
              items: [
                {
                  resourceURI:
                    "http://gateway.marvel.com/v1/public/characters/1009257",
                  name: "Cyclops",
                },
                {
                  resourceURI:
                    "http://gateway.marvel.com/v1/public/characters/1009313",
                  name: "Gambit",
                },
                {
                  resourceURI:
                    "http://gateway.marvel.com/v1/public/characters/1009337",
                  name: "Havok",
                },
                {
                  resourceURI:
                    "http://gateway.marvel.com/v1/public/characters/1009465",
                  name: "Mystique",
                },
                {
                  resourceURI:
                    "http://gateway.marvel.com/v1/public/characters/1009499",
                  name: "Polaris",
                },
                {
                  resourceURI:
                    "http://gateway.marvel.com/v1/public/characters/1009546",
                  name: "Rogue",
                },
                {
                  resourceURI:
                    "http://gateway.marvel.com/v1/public/characters/1009726",
                  name: "X-Men",
                },
              ],
              returned: 7,
            },
            series: {
              available: 2,
              collectionURI:
                "http://gateway.marvel.com/v1/public/stories/660/series",
              items: [
                {
                  resourceURI: "http://gateway.marvel.com/v1/public/series/1643",
                  name: "Decimation: X-Men the Day After (2006)",
                },
                {
                  resourceURI: "http://gateway.marvel.com/v1/public/series/403",
                  name: "X-Men (2004 - 2007)",
                },
              ],
              returned: 2,
            },
            comics: {
              available: 2,
              collectionURI:
                "http://gateway.marvel.com/v1/public/stories/660/comics",
              items: [
                {
                  resourceURI: "http://gateway.marvel.com/v1/public/comics/4072",
                  name: "Decimation: X-Men the Day After (Trade Paperback)",
                },
                {
                  resourceURI: "http://gateway.marvel.com/v1/public/comics/3364",
                  name: "X-Men (2004) #181",
                },
              ],
              returned: 2,
            },
            events: {
              available: 0,
              collectionURI:
                "http://gateway.marvel.com/v1/public/stories/660/events",
              items: [],
              returned: 0,
            },
            originalIssue: {
              resourceURI: "http://gateway.marvel.com/v1/public/comics/3364",
              name: "X-Men (2004) #181",
            },
          },
      ],
    },
  };
  