import { combineReducers } from "redux";
import  CharacterListReducer from "./CharacterListReducer";
import CharacterMultipleReducer from "./CharacterMultipleReducer";
import ComicListReducer from "./ComicListReducer";
import StoryListReducer from "./StoryListReducer";
import BookmarkReducer from "./BookmarkReducer";
import ComicMultipleReducer from "./ComicMultipleReducer";
import { persistReducer } from "redux-persist";
import storage from "redux-persist/lib/storage";
import StoryMultipleReducer from "./StoryMultipleReducer";

const persistConfig = {
    key: 'root',
    storage,
    whitelist: ['bookmarks']
}

const RootReducer = combineReducers({
    CharacterList: CharacterListReducer,
    Character: CharacterMultipleReducer,
    ComicList: ComicListReducer,
    Comic: ComicMultipleReducer,
    StoryList: StoryListReducer,
    Story: StoryMultipleReducer,
    bookmarks: BookmarkReducer
});

export type RootState = ReturnType<typeof RootReducer>;

export default persistReducer(persistConfig, RootReducer);