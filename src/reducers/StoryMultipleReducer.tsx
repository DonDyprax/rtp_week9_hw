const initialState = {
    loading: false,
    data: {},
    errorMsg: ""
}

const StoryMultipleReducer = (state = initialState, action: {type: string, payload?: any, story?: any}) => {
    const { type } = action;

    switch(type) {
        case "STORY_MULTIPLE_LOADING": {
            return {
                ...state,
                loading: true,
                errorMsg: ""
            }
        }

        case "STORY_MULTIPLE_FAIL": {
            return {
                ...state,
                loading: false,
                errorMsg: "Unable to find story"
            }
        }

        case "STORY_MULTIPLE_SUCCESS": {
            return {
                ...state,
                loading: false,
                errorMsg: "",
                data: {
                    ...state.data,
                    [action.story]: action.payload
                }
            }
        }

        default: {
            return state;
        }
    }

}

export default StoryMultipleReducer;