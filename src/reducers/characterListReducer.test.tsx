import CharacterListReducer from "./CharacterListReducer";

it('Character List Reducer returns initial state', () => {
    expect(CharacterListReducer(undefined, {type: ""})).toEqual({
        loading: false,
        data: [],
        errorMsg: "",
        total: 1493
    })
});

it('Character List loading state is working', () => {
    expect(CharacterListReducer(undefined, {type: "CHARACTER_LIST_LOADING"})).toEqual({
        data: [],
        errorMsg: "",
        loading: true,
        total: 1493
    })
});

it('Character List success is working', () => {
    expect(CharacterListReducer(undefined, {type: "CHARACTER_LIST_SUCCESS", payload: {test: true}})).toEqual({
        loading: false,
        errorMsg: "",
        data: {test: true},
        total: 1493
    })
});

it('Character List failure is working', () => {
    expect(CharacterListReducer(undefined, {type: "CHARACTER_LIST_FAIL"})).toEqual({
        data: [],
        loading: false,
        errorMsg: "Unable to load the list of characters.",
        total: 1493
    })
})