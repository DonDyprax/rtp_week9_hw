import CharacterMultipleReducer from "./CharacterMultipleReducer";

it('Character Multiple Reducer returns initial state', () => {
    expect(CharacterMultipleReducer(undefined, {type: ""})).toEqual({
        loading: false,
        data: {},
        errorMsg: ""
    })
});

it('Character Multiple loading state is working', () => {
    expect(CharacterMultipleReducer(undefined, {type: "CHARACTER_MULTIPLE_LOADING"})).toEqual({
        data: {},
        errorMsg: "",
        loading: true
    })
});

it('Character Multiple success is working', () => {
    expect(CharacterMultipleReducer(undefined, {type: "CHARACTER_MULTIPLE_SUCCESS", payload: {test: true}, character: "testCharacter"})).toEqual({
        loading: false,
        errorMsg: "",
        data: {testCharacter: {test: true}}
    })
});

it('Character Multiple failure is working', () => {
    expect(CharacterMultipleReducer(undefined, {type: "CHARACTER_MULTIPLE_FAIL"})).toEqual({
        data: {},
        loading: false,
        errorMsg: "Unable to find character"
    })
})