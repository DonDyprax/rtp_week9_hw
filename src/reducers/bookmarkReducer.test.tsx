import BookmarkReducer from "./BookmarkReducer";

it('Bookmark Reducer returns initial state', () => {
    expect(BookmarkReducer(undefined, {type: ""})).toEqual({
        loading: false,
        data: [{}],
        errorMsg: ""
    })
});

it('Bookmark loading state is working', () => {
    expect(BookmarkReducer(undefined, {type: "BOOKMARK_LOADING"})).toEqual({
        data: [{}],
        errorMsg: "",
        loading: true
    })
});

it('Bookmark success is working', () => {
    expect(BookmarkReducer(undefined, {type: "BOOKMARK_SUCCESS", payload: [{test: true}]})).toEqual({
        loading: false,
        errorMsg: "",
        data: [{test: true}]
    })
});

it('Bookmark failure is working', () => {
    expect(BookmarkReducer(undefined, {type: "BOOKMARK_FAIL"})).toEqual({
        data: [{}],
        loading: false,
        errorMsg: "Unable to bookmark item."
    })
})