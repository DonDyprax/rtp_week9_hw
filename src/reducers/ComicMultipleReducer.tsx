
const initialState = {
    loading: false,
    data: {},
    errorMsg: ""
}

const ComicMultipleReducer = (state = initialState, action: {type: string, payload?: any, comic?: any}) => {
    const { type } = action;

    switch(type) {
        case "COMIC_MULTIPLE_LOADING": {
            return {
                ...state,
                loading: true,
                errorMsg: ""
            }
        }

        case "COMIC_MULTIPLE_FAIL": {
            return {
                ...state,
                loading: false,
                errorMsg: "Unable to find comic"
            }
        }

        case "COMIC_MULTIPLE_SUCCESS": {
            return {
                ...state,
                loading: false,
                errorMsg: "",
                data: {
                    ...state.data,
                    [action.comic]: action.payload
                }
            }
        }

        default: {
            return state;
        }
    }

}

export default ComicMultipleReducer;