
const initialState = {
    loading: false,
    data: [],
    errorMsg: "",
    total: 113235
};

const StoryListReducer = (state = initialState, action: {type: string, payload?: any}) => {
    const { type } = action;

    switch (type) {

        case "STORY_LIST_LOADING": {
            return {
                ...state,
                loading: true
            };
        }

        case "STORY_LIST_SUCCESS": {
            return {
                ...state,
                loading: false,
                data: action.payload,
                errorMsg: ""
            };
        }

        case "STORY_LIST_FAIL": {
            return {
                ...state,
                loading: false,
                errorMsg: "Unable to load the list of stories."
            };
        }

        default: {
            return state;
        }
    }
}

export default StoryListReducer;