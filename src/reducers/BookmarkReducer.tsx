
const initialState = {
    loading: false,
    data: [{}],
    errorMsg: ""
};

const BookmarkReducer = (state = initialState, action: {type: string, payload?: any}) => {
    const { type } = action;

    switch (type) {

        case "BOOKMARK_LOADING": {
            return {
                ...state,
                loading: true
            };
        }

        case "BOOKMARK_SUCCESS": {
            console.log(action.payload);
            return {
                ...state,
                loading: false,
                data: action.payload,
                errorMsg: ""
            };
        }

        case "BOOKMARK_FAIL": {
            return {
                ...state,
                loading: false,
                errorMsg: "Unable to bookmark item."
            };
        }

        default: {
            return state;
        }
    }
}

export default BookmarkReducer;