
const initialState = {
    loading: false,
    data: {},
    errorMsg: ""
}

const CharacterMultipleReducer = (state = initialState, action: {type: string, payload?: any, character?: any}) => {
    const { type } = action;

    switch(type) {
        case "CHARACTER_MULTIPLE_LOADING": {
            return {
                ...state,
                loading: true,
                errorMsg: ""
            }
        }

        case "CHARACTER_MULTIPLE_FAIL": {
            return {
                ...state,
                loading: false,
                errorMsg: "Unable to find character"
            }
        }

        case "CHARACTER_MULTIPLE_SUCCESS": {
            return {
                ...state,
                loading: false,
                errorMsg: "",
                data: {
                    ...state.data,
                    [action.character]: action.payload
                }
            }
        }

        default: {
            return state;
        }
    }

}

export default CharacterMultipleReducer;