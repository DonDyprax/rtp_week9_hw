import StoryListReducer from "./StoryListReducer";

it('Story List Reducer returns initial state', () => {
    expect(StoryListReducer(undefined, {type: ""})).toEqual({
        loading: false,
        data: [],
        errorMsg: "",
        total: 113235
    })
});

it('Story List loading state is working', () => {
    expect(StoryListReducer(undefined, {type: "STORY_LIST_LOADING"})).toEqual({
        data: [],
        errorMsg: "",
        loading: true,
        total: 113235
    })
});

it('Story List success is working', () => {
    expect(StoryListReducer(undefined, {type: "STORY_LIST_SUCCESS", payload: {test: true}})).toEqual({
        loading: false,
        errorMsg: "",
        data: {test: true},
        total: 113235
    })
});

it('Story List failure is working', () => {
    expect(StoryListReducer(undefined, {type: "STORY_LIST_FAIL"})).toEqual({
        data: [],
        loading: false,
        errorMsg: "Unable to load the list of stories.",
        total: 113235
    })
})