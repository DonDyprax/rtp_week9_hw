import ComicMultipleReducer from "./ComicMultipleReducer";

it('Comic Multiple Reducer returns initial state', () => {
    expect(ComicMultipleReducer(undefined, {type: ""})).toEqual({
        loading: false,
        data: {},
        errorMsg: ""
    })
});

it('Comic Multiple loading state is working', () => {
    expect(ComicMultipleReducer(undefined, {type: "COMIC_MULTIPLE_LOADING"})).toEqual({
        data: {},
        errorMsg: "",
        loading: true
    })
});

it('Comic Multiple success is working', () => {
    expect(ComicMultipleReducer(undefined, {type: "COMIC_MULTIPLE_SUCCESS", payload: {test: true}, comic: "testComic"})).toEqual({
        loading: false,
        errorMsg: "",
        data: {testComic: {test: true}}
    })
});

it('Comic Multiple failure is working', () => {
    expect(ComicMultipleReducer(undefined, {type: "COMIC_MULTIPLE_FAIL"})).toEqual({
        data: {},
        loading: false,
        errorMsg: "Unable to find comic"
    })
})