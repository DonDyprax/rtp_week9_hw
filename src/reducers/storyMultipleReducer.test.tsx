import StoryMultipleReducer from "./StoryMultipleReducer";

it('Story Multiple Reducer returns initial state', () => {
    expect(StoryMultipleReducer(undefined, {type: ""})).toEqual({
        loading: false,
        data: {},
        errorMsg: ""
    })
});

it('Story Multiple loading state is working', () => {
    expect(StoryMultipleReducer(undefined, {type: "STORY_MULTIPLE_LOADING"})).toEqual({
        data: {},
        errorMsg: "",
        loading: true
    })
});

it('Story Multiple success is working', () => {
    expect(StoryMultipleReducer(undefined, {type: "STORY_MULTIPLE_SUCCESS", payload: {test: true}, story: "testStory"})).toEqual({
        loading: false,
        errorMsg: "",
        data: {testStory: {test: true}}
    })
});

it('Story Multiple failure is working', () => {
    expect(StoryMultipleReducer(undefined, {type: "STORY_MULTIPLE_FAIL"})).toEqual({
        data: {},
        loading: false,
        errorMsg: "Unable to find story"
    })
})