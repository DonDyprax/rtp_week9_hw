
const initialState = {
    loading: false,
    data: [],
    errorMsg: "",
    total: 1493
};

const CharacterListReducer = (state = initialState, action: {type: string, payload?: any}) => {
    const { type } = action;

    switch (type) {

        case "CHARACTER_LIST_LOADING": {
            return {
                ...state,
                loading: true
            };
        }

        case "CHARACTER_LIST_SUCCESS": {
            return {
                ...state,
                loading: false,
                data: action.payload,
                errorMsg: ""
            };
        }

        case "CHARACTER_LIST_FAIL": {
            return {
                ...state,
                loading: false,
                errorMsg: "Unable to load the list of characters."
            };
        }

        default: {
            return state;
        }
    }
}

export default CharacterListReducer;