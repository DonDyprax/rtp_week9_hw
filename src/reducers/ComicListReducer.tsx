
const initialState = {
    loading: false,
    data: [],
    errorMsg: "",
    total: 48544
};

const ComicListReducer = (state = initialState, action: {type: string, payload?: any}) => {
    const { type } = action;

    switch (type) {

        case "COMIC_LIST_LOADING": {
            return {
                ...state,
                loading: true
            };
        }

        case "COMIC_LIST_SUCCESS": {
            return {
                ...state,
                loading: false,
                data: action.payload,
                errorMsg: ""
            };
        }

        case "COMIC_LIST_FAIL": {
            return {
                ...state,
                loading: false,
                errorMsg: "Unable to load the list of comics."
            };
        }

        default: {
            return state;
        }
    }
}

export default ComicListReducer;