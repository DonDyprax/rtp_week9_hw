import ComicListReducer from "./ComicListReducer";

it('Comic List Reducer returns initial state', () => {
    expect(ComicListReducer(undefined, {type: ""})).toEqual({
        loading: false,
        data: [],
        errorMsg: "",
        total: 48544
    })
});

it('Comic List loading state is working', () => {
    expect(ComicListReducer(undefined, {type: "COMIC_LIST_LOADING"})).toEqual({
        data: [],
        errorMsg: "",
        loading: true,
        total: 48544
    })
});

it('Comic List success is working', () => {
    expect(ComicListReducer(undefined, {type: "COMIC_LIST_SUCCESS", payload: {test: true}})).toEqual({
        loading: false,
        errorMsg: "",
        data: {test: true},
        total: 48544
    })
});

it('Comic List failure is working', () => {
    expect(ComicListReducer(undefined, {type: "COMIC_LIST_FAIL"})).toEqual({
        data: [],
        loading: false,
        errorMsg: "Unable to load the list of comics.",
        total: 48544
    })
})