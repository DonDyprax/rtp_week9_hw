import React from "react";
import "./App.css";
import "./styles/css/style.css";
import Routes from "./config/Routes";

function App() {
  return (
    <>
      <Routes />
    </>
  );
}

export default App;
