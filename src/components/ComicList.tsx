import React, { useEffect, useCallback, useState, useRef } from "react";
import { useDispatch, useSelector } from "react-redux";
import {
  getComicList,
  getComicStories,
  getComicListByTitle,
  getComicsByFormat,
  getComicCharacters,
} from "../actions/comicActions";
import Card from "./Card";
import ReactPaginate from "react-paginate";
import _, { debounce } from "lodash";

const ComicList = () => {
  const [search, setSearch] = useState("");
  const filterRef = useRef<HTMLSelectElement>(null);
  const searchRef = useRef<HTMLButtonElement>(null);
  const dispatch = useDispatch();
  const comicList = useSelector((state: any) => state.ComicList);

  const fetchComicList = useCallback(
    (page: number = 1) => {
      dispatch(getComicList(page));
    },
    [dispatch]
  );

  const fetchComicListByTitle = useCallback(
    (value: string) => {
      dispatch(getComicListByTitle(value));
    },
    [dispatch]
  );

  const fetchComicsListByFormat = useCallback(
    (value: string) => {
      dispatch(getComicsByFormat(value));
    },
    [dispatch]
  );

  const fetchComicCharacters = useCallback(
    (value: string) => {
      dispatch(getComicCharacters(value));
    },
    [dispatch]
  );

  const fetchComicStories = useCallback(
    (value: string) => {
      dispatch(getComicStories(value));
    },
    [dispatch]
  );

  const submitDebounced = debounce(
    () => {
      if(searchRef && searchRef.current) {
        searchRef.current.click();
      }
    }
  , 1500);

  const selectFilterMethod = useCallback(
    (value: string) => {
      switch (value) {
        case "title": {
          if (search === "") {
            return fetchComicList(1);
          }

          return fetchComicListByTitle(search);
        }

        case "format": {
          return fetchComicsListByFormat(search);
        }

        case "characters": {
          return fetchComicCharacters(search);
        }

        case "stories": {
          return fetchComicStories(search);
        }

        default: {
          return fetchComicList(1);
        }
      }
    },
    [fetchComicCharacters, fetchComicStories, fetchComicList, fetchComicsListByFormat, fetchComicListByTitle, search]
  );

  useEffect(() => {
    fetchComicList(1);
  }, [fetchComicList]);

  const showData = () => {
    if (comicList.data !== []) {
      return (
        <div data-testid="comicsList" className="comics-list">
          {comicList.data?.data?.results.map((item: any) => {
            return <Card key={item.id} data={item} />;
          })}
        </div>
      );
    }

    if (comicList.loading) {
      return <h1>Loading...</h1>;
    }

    if (comicList.errorMsg !== "") {
      return <h1>{comicList.errorMsg}</h1>;
    }

    return <h1>Unable to get data</h1>;
  };

  useEffect(() => {}, []);

  return (
    <div>
      <div className="search-container">
        <label htmlFor="filters">Filter by: </label>
        <select ref={filterRef} name="filters" id="filters">
          <option value="title">Title</option>
          <option value="format">Format</option>
          <option value="characters">Characters</option>
          <option value="stories">Stories</option>
        </select>

        <p>Search: </p>
        <input type="text" onChange={(e) => {
            setSearch(e.target.value);
            submitDebounced();
            }} />
        <button
          ref={searchRef}
          onClick={() => {
            if (filterRef && filterRef.current) {
              selectFilterMethod(filterRef.current.value);
            }
          }}
        >
          Search
        </button>
      </div>
      {showData()}
      {!_.isEmpty(comicList.data) && (
        <ReactPaginate 
        pageCount={Math.ceil(comicList.total / 12)}
        pageRangeDisplayed={2}
        marginPagesDisplayed={1}
        onPageChange={(data) => fetchComicList(data.selected + 1)}
        containerClassName={'pagination-container'}
      />
      )}
    </div>
  );
};

export default ComicList;
