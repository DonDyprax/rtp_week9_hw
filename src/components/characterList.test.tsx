import CharacterList from "./CharacterList";
import { render, screen, waitFor, waitForElementToBeRemoved } from "@testing-library/react";
import { Provider } from "react-redux";
import { Store, persistor } from "../config/Store";
import { PersistGate } from "redux-persist/integration/react";
import { BrowserRouter } from "react-router-dom";
import user from '@testing-library/user-event';
import { act } from "react-dom/test-utils";



describe("Character List Component testing", () => {
    it('Charcter List loads renders', () => {
      render(
        <Provider store={Store}>
        <BrowserRouter>
          <PersistGate persistor={persistor}>
            <CharacterList />
          </PersistGate>
        </BrowserRouter>
      </Provider>
      )

      screen.debug();
  
    })
    

  it("Character List loads properly", async () => {
    render(
      <Provider store={Store}>
        <BrowserRouter>
          <PersistGate persistor={persistor}>
            <CharacterList />
          </PersistGate>
        </BrowserRouter>
      </Provider>
    );
    await waitFor(() => expect(screen.getByText('3-D Man')).toBeInTheDocument());
    await waitFor(() => expect(screen.getByText('A-Bomb (HAS)')).toBeInTheDocument());
    await waitFor(() => expect(screen.getByText('A.I.M.')).toBeInTheDocument());
  });
});


it('Character List search by name working', async () => {
  
    render(
        <Provider store={Store}>
          <BrowserRouter>
            <PersistGate persistor={persistor}>
              <CharacterList />
            </PersistGate>
          </BrowserRouter>
        </Provider>
      );
/*
      const selection = screen.getByTestId('filterSelect');
      userEvent.selectOptions(selection, 'Name');
      userEvent.click(screen.getByText('Name'));
*/
      act(() => {
        const input = screen.getByTestId('searchInput');
        const button = screen.getByTestId('searchButton');
        user.type(input, 'A-B');
        user.click(button);
      });

      //await waitForElementToBeRemoved(() => screen.getByText('3-D Man'));
      screen.debug();   
})

it('Character List search by character comics working', async () => {
  
  render(
      <Provider store={Store}>
        <BrowserRouter>
          <PersistGate persistor={persistor}>
            <CharacterList />
          </PersistGate>
        </BrowserRouter>
      </Provider>
    );

    const selection = screen.getByTestId('filterSelect');
    user.selectOptions(selection, 'characterComics');
    user.click(screen.getByText('Character Comics'));

    act(() => {
      const input = screen.getByTestId('searchInput');
      const button = screen.getByTestId('searchButton');
      user.type(input, '3-D Man');
      user.click(button);
    });

    await waitFor(() => {
      expect(screen.getByText('Avengers: The Initiative (2007) #19')).toBeInTheDocument();
    })
})