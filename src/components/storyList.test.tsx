import StoryList from "./StoryList";
import { render, screen, waitFor, waitForElementToBeRemoved } from "@testing-library/react";
import { Provider } from "react-redux";
import { Store, persistor } from "../config/Store";
import { PersistGate } from "redux-persist/integration/react";
import { BrowserRouter } from "react-router-dom";


describe("Story List Component testing", () => {
    it('Story List renders properly', () => {
      render(
        <Provider store={Store}>
        <BrowserRouter>
          <PersistGate persistor={persistor}>
            <StoryList />
          </PersistGate>
        </BrowserRouter>
      </Provider>
      )
  
    })
    

  it("Story List loads properly", async () => {
    render(
      <Provider store={Store}>
        <BrowserRouter>
          <PersistGate persistor={persistor}>
            <StoryList />
          </PersistGate>
        </BrowserRouter>
      </Provider>
    );
    
    await waitFor(() => expect(screen.getByText("1 of 2 - What Lorna Saw")).toBeInTheDocument());
    await waitFor(() => expect(screen.getByText("X-MEN (2004) #181")).toBeInTheDocument());
    await waitFor(() => expect(screen.getByText("2 of 2 - What Lorna Saw")).toBeInTheDocument());
  });
});