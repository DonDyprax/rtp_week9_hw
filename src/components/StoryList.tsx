import React, { useEffect, useCallback, useState, useRef } from "react";
import { useDispatch, useSelector } from "react-redux";
import {
  getStoryList,
  getStoryCharacters,
  getStoryComics,
} from "../actions/storyActions";
import Card from "./Card";
import ReactPaginate from "react-paginate";
import _, { debounce } from "lodash";

const StoryList = () => {
  const [search, setSearch] = useState("");
  const filterRef = useRef<HTMLSelectElement>(null);
  const searchRef = useRef<HTMLButtonElement>(null);
  const dispatch = useDispatch();
  const storyList = useSelector((state: any) => state.StoryList);

  const fetchStoryList = useCallback(
    (page: number = 1) => {
      dispatch(getStoryList(page));
    },
    [dispatch]
  );

  const fetchStoryCharacters = useCallback(
    (value: string) => {
      dispatch(getStoryCharacters(value));
    },
    [dispatch]
  );

  const fetchStoryComics = useCallback(
    (value: string) => {
      dispatch(getStoryComics(value));
    },
    [dispatch]
  );

  const submitDebounced = debounce(
    () => {
      if(searchRef && searchRef.current) {
        searchRef.current.click();
      }
    }
  , 1500);

  const selectFilterMethod = useCallback(
    (value: string) => {
      switch (value) {
        case "characters": {
          return fetchStoryCharacters(search);
        }

        case "comics": {
          return fetchStoryComics(search);
        }

        default: {
          return fetchStoryList(1);
        }
      }
    },
    [fetchStoryList, fetchStoryCharacters, fetchStoryComics, search]
  );

  useEffect(() => {
    fetchStoryList(1);
  }, [fetchStoryList]);

  const showData = () => {
    if (storyList.data !== []) {
      return (
        <div data-testid="storiesList" className="story-list">
          {storyList.data?.data?.results.map((item: any) => {
            return <Card key={item.id} data={item} />;
          })}
        </div>
      );
    }

    if (storyList.loading) {
      return <h1>Loading...</h1>;
    }

    if (storyList.errorMsg !== "") {
      return <h1>{storyList.errorMsg}</h1>;
    }

    return <h1>Unable to get data</h1>;
  };

  useEffect(() => {}, []);

  return (
    <div>
      <div className="search-container">
        <label htmlFor="filters">Filter by: </label>
        <select ref={filterRef} name="filters" id="filters">
          <option value="characters">Characters</option>
          <option value="comics">Comics</option>
        </select>

        <p>Search: </p>
        <input
          type="text"
          onChange={(e) => {
            setSearch(e.target.value);
            submitDebounced();
          }}
        />
        <button
            ref={searchRef}
          onClick={() => {
            if (filterRef && filterRef.current) {
              selectFilterMethod(filterRef.current.value);
            }
          }}
        >
          Search
        </button>
      </div>
      {showData()}
      {!_.isEmpty(storyList.data) && (
        <ReactPaginate 
        pageCount={Math.ceil(storyList.total / 12)}
        pageRangeDisplayed={2}
        marginPagesDisplayed={1}
        onPageChange={(data) => fetchStoryList(data.selected + 1)}
        containerClassName={'pagination-container'}
      />
      )}
    </div>
  );
};

export default StoryList;
