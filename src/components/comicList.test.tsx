import ComicList from "./ComicList";
import { render, screen, waitFor, waitForElementToBeRemoved } from "@testing-library/react";
import { Provider } from "react-redux";
import { Store, persistor } from "../config/Store";
import { PersistGate } from "redux-persist/integration/react";
import { BrowserRouter } from "react-router-dom";


describe("Comic List Component testing", () => {
    it('Comic List renders properly', () => {
      render(
        <Provider store={Store}>
        <BrowserRouter>
          <PersistGate persistor={persistor}>
            <ComicList />
          </PersistGate>
        </BrowserRouter>
      </Provider>
      )
  
    })
    

  it("Comic List loads properly", async () => {
    render(
      <Provider store={Store}>
        <BrowserRouter>
          <PersistGate persistor={persistor}>
            <ComicList />
          </PersistGate>
        </BrowserRouter>
      </Provider>
    );
    
    await waitFor(() => expect(screen.getByText("Ultimate Spider-Man Ultimate Collection Book 1 (Trade Paperback)")).toBeInTheDocument());
    await waitFor(() => expect(screen.getByText("Ant-Man (2003) #1")).toBeInTheDocument());
    await waitFor(() => expect(screen.getByText("ULTIMATE X-MEN VOL. 5: ULTIMATE WAR TPB (Trade Paperback)")).toBeInTheDocument());
  });
});
