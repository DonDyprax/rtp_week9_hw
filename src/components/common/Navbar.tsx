import React from "react";
import { Link } from "react-router-dom";

const Navbar = () => {
  return (
    <div className="navbar-container">
      <div className="navbar-logo">
        <Link to="/">
          <img
            src="https://upload.wikimedia.org/wikipedia/commons/b/b9/Marvel_Logo.svg"
            alt="Logo"
          />
        </Link>
      </div>
      <ul className="navbar-links">
        <li>
          <Link to="/characters">Characters</Link>
        </li>
        <li>
          <Link to="/comics">Comics</Link>
        </li>
        <li>
          <Link to="/stories">Stories</Link>
        </li>
      </ul>
    </div>
  );
};

export default React.memo(Navbar);
