import React, { useEffect, useCallback, useState, useRef } from "react";
import { useDispatch, useSelector } from "react-redux";
import { RootState } from "../reducers/RootReducer";
import {
  getCharacterList,
  getCharacterListByName,
  getCharactersByComic,
  getCharacterComics,
  getCharacterStories
} from "../actions/characterActions";
import Character from "../interfaces/Character";
import Card from "./Card";
import ReactPaginate from "react-paginate";
import _, { debounce } from "lodash";

const CharacterList = () => {
  const [search, setSearch] = useState("");
  const filterRef = useRef<HTMLSelectElement>(null);
  const searchRef = useRef<HTMLButtonElement>(null);
  const dispatch = useDispatch();
  const characterList = useSelector((state: RootState) => state.CharacterList);

  const fetchCharacterList = useCallback(
    (page: number = 1) => {
      dispatch(getCharacterList(page));
    },
    [dispatch]
  );

  const fetchCharacterListByName = useCallback(
    (name: string) => {
      dispatch(getCharacterListByName(name));
    },
    [dispatch]
  );

  const fetchCharactersByComics = useCallback(
    (name: string) => {
      dispatch(getCharactersByComic(name));
    },
    [dispatch]
  );

  const fetchCharacterComics = useCallback(
    (name: string) => {
      dispatch(getCharacterComics(name));
    },
    [dispatch]
  );

  const fetchCharacterStories = useCallback(
    (name: string) => {
      dispatch(getCharacterStories(name));
    },
    [dispatch]
  )

  const submitDebounced = debounce(
    () => {
      if(searchRef && searchRef.current) {
        searchRef.current.click();
      }
    }
  , 3000);

  const selectFilterMethod = useCallback(
    (value: string) => {
      switch (value) {
        case "comics": {
          return fetchCharactersByComics(search);
        }

        case "characterComics": {
          console.log("characterComics");
          return fetchCharacterComics(search);
        }

        case "characterStories": {
          console.log("characterStories");
          return fetchCharacterStories(search);
        }

        case "name": {
          if (search === "") {
            return fetchCharacterList(1);
          }

          return fetchCharacterListByName(search);
        }

        default: {
          return fetchCharacterList(1);
        }
      }
    },
    [
      fetchCharacterList,
      fetchCharacterListByName,
      fetchCharacterComics,
      fetchCharactersByComics,
      fetchCharacterStories,
      search,
    ]
  );

  const showData = () => {
    if (characterList.data.length !== 0) {
      return (
        <div data-testid="charactersList" className="characters-list">
          {characterList.data?.data?.results.map((char: Character) => {
            return <Card key={char.id} data={char} />
          })}
        </div>
      );
    }

    if (characterList.loading) {
      return <h1>Loading...</h1>;
    }

    if (characterList.errorMsg !== "") {
      return <h1>{characterList.errorMsg}</h1>;
    }

    return <h1>Unable to getdata</h1>;
  };

  useEffect(() => {
    fetchCharacterList(1);
  }, [fetchCharacterList]);

  return (
    <div>
      <div className="search-container">
        <label htmlFor="filters">Filter by: </label>
        <select data-testid='filterSelect' ref={filterRef} name="filters" id="filters">
          <option value="name">Name</option>
          <option value="comics">Comics</option>
          <option value="stories">Stories</option>
          <option value="characterComics">Character Comics</option>
          <option value="characterStories">Character Stories</option>
        </select>

        <p>Search: </p>
        <input data-testid='searchInput' type="text" onChange={(e) => {
          setSearch(e.target.value);
          submitDebounced();
          }} />
        <button
          ref={searchRef}
          data-testid='searchButton'
          onClick={() => {
            if (filterRef && filterRef.current) {
              selectFilterMethod(filterRef.current.value);
            }
          }}
        >
          Search
        </button>
      </div>
      {showData()}
      {!_.isEmpty(characterList.data) && (
        <ReactPaginate 
        pageCount={Math.ceil(characterList.total / 12)}
        pageRangeDisplayed={2}
        marginPagesDisplayed={1}
        onPageChange={(data) => fetchCharacterList(data.selected + 1)}
        containerClassName={'pagination-container'}
      />
      )}
    </div>
  );
};

export default CharacterList;
