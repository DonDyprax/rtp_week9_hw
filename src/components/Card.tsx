import React, { useCallback } from "react";
import { Link } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";
import { checkBookmark } from "../actions/bookmarkActions";

interface Props {
  data: any;
}

const Card: React.FC<Props> = ({ data }) => {
  let type;
  const dispatch = useDispatch();
  const bookmarks = useSelector((state: any) => state.bookmarks);

  const bookmark = useCallback(
    (data, action) => {
      dispatch(checkBookmark(data, action));
    },
    [dispatch]
  );

  if (!("comics" in data)) {
    type = "comic";
  } else if (!("characters" in data)) {
    type = "character";
  } else {
    type = "story";
  } 

  if (type === "character") {
    return (
      <div data-testid='card'className="character-card">
        <Link to={`/characters/${data.id}`}>
          <div className="character-image">
            <img
              src={
                data.thumbnail.path +
                "/portrait_medium." +
                data.thumbnail.extension
              }
              alt="Character"
            />
          </div>
          <div className="character-details">
            <p>{data.name}</p>
          </div>
        </Link>
        <div className="bookmark">
            <button onClick={() => bookmark(data, bookmarks.data)}>Fav</button>
        </div>
      </div>
    );
  } else if (type === "comic") {
    return (
      <Link to={`/comics/${data.id}`}>
        <div data-testid='card'className="comic-card">
          <div className="comic-image">
            <img
              src={
                data.thumbnail.path +
                "/portrait_medium." +
                data.thumbnail.extension
              }
              alt="Character"
            />
          </div>
          <div className="comic-details">
            <p>{data.title}</p>
          </div>
        </div>
      </Link>
    );
  } else {
    return (
      <Link to={`/stories/${data.id}`}>
        <div data-testid='card'className="story-card">
          <div className="story-details">
            <p>{data.title}</p>
          </div>
        </div>
      </Link>
    );
  }
};

export default Card;
