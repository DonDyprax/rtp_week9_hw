import Story from "./Story";
import { render, screen, waitFor, waitForElementToBeRemoved } from "@testing-library/react";
import { Provider } from "react-redux";
import { Store, persistor } from "../config/Store";
import { PersistGate } from "redux-persist/integration/react";
import { BrowserRouter } from "react-router-dom";


describe("Story Page testing", () => {
    it('Story Page loads properly', async () => {
      render(
        <Provider store={Store}>
        <BrowserRouter>
          <PersistGate persistor={persistor}>
            <Story />
          </PersistGate>
        </BrowserRouter>
      </Provider>
      )

      await waitForElementToBeRemoved(() => screen.queryByText(/Loading.../i));
      expect(screen.getByTestId('storyContainer')).toBeInTheDocument();
    screen.debug();
    })
})