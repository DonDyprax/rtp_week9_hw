import React from "react";
import CharacterList from "../components/CharacterList";

const Characters = () => {

  return (
    <div className="characters">
      <div data-testid="caractersContainer" className="characters-container">
        <CharacterList />
      </div>
    </div>
  );
};

export default Characters;
