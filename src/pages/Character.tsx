import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { getCharacter } from "../actions/characterActions";
import _ from "lodash";
import { Link } from "react-router-dom";

const Character = () => {
  const id = "1011334";

  const dispatch = useDispatch();
  const characterState = useSelector((state: any) => state.Character);

  useEffect(() => {
    dispatch(getCharacter(1011334));
  }, [dispatch, id]);

  const showData = () => {
    if (!_.isEmpty(characterState.data[id])) {
      const charData = characterState.data[id].data.results[0];
      console.log(charData);
      return (
        <div data-testid='characterContainer'className="character-container">
          <h1>{charData.name}</h1>
          <img
            src={`${charData.thumbnail.path}/portrait_uncanny.${charData.thumbnail.extension}`}
            alt=""
          />
          <p>{charData.description}</p>

          <div className="character-comics">
            <h1>Comics this character appears in</h1>
            {charData.comics.items.map((comic: any) => {
              const path = comic.resourceURI.split("/")[6];

              return (
                <Link to={`/comics/${path}`}>
                  <p>{comic.name}</p>
                </Link>
              );
            })}
          </div>

          <div className="character-stories">
              <h1>Stories this character appears in</h1>
            {charData.stories.items.map((story: any) => {
              console.log(story);
              const path = story.resourceURI.split("/")[6];

              return (
                <Link to={`/stories/${path}`}>
                  <p>{story.name}</p>
                </Link>
              );
            })}
          </div>
        </div>
      );
    }

    if (characterState.loading) {
      return <h1>Loading...</h1>;
    }

    if (characterState.errorMsg !== "") {
      return <h1>{characterState.errorMsg}</h1>;
    }

    return <h1>Error getting character</h1>;
  };

  return <div className={"character"}>{showData()}</div>;
};

export default Character;
