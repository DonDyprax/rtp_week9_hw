import Characters from "./Characters";
import {  render, screen, waitForElementToBeRemoved } from "@testing-library/react";
import { Provider } from "react-redux";
import { Store, persistor } from "../config/Store";
import { PersistGate } from "redux-persist/integration/react";
import { BrowserRouter } from "react-router-dom";

it("Characters List page is rendering", async () => {
  render(
    <Provider store={Store}>
      <BrowserRouter>
        <PersistGate persistor={persistor}>
          <Characters />
        </PersistGate>
      </BrowserRouter>
    </Provider>
  );

  await waitForElementToBeRemoved(() => screen.queryByText(/Loading.../i));
  expect(screen.getByTestId('charactersList')).toBeInTheDocument();
});

