import Comic from "./Comic";
import { render, screen, waitFor, waitForElementToBeRemoved } from "@testing-library/react";
import { Provider } from "react-redux";
import { Store, persistor } from "../config/Store";
import { PersistGate } from "redux-persist/integration/react";
import { BrowserRouter } from "react-router-dom";


describe("Comic Page testing", () => {
    it('Comic Page loads properly', async () => {
      render(
        <Provider store={Store}>
        <BrowserRouter>
          <PersistGate persistor={persistor}>
            <Comic />
          </PersistGate>
        </BrowserRouter>
      </Provider>
      )

      await waitForElementToBeRemoved(() => screen.queryByText(/Loading.../i));
      expect(screen.getByTestId('comicContainer')).toBeInTheDocument();
    screen.debug();
    })
})