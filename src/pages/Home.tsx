import React from "react";

const Home = () => {
  return (
    <div className="home">
      <div className="home-container">
        <h1>Welcome to a mock Marvel Database</h1>
        <p>This application utilizes Marvel's API to get all of the Characters, Comics and Stories</p>
      </div>
    </div>
  );
};

export default Home;
