import React from "react";
import ComicList from "../components/ComicList";

const Comics = () => {
  return (
    <div className="comics">
      <div data-testid="comicsContainer" className="comics-container">
        <ComicList />
      </div>
    </div>
  );
};

export default Comics;
