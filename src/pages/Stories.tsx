import React from "react";
import StoryList from "../components/StoryList";

const Stories = () => {
  return (
    <div className="stories">
      <div data-testid="storiesContainer" className="stories-container">
        <StoryList />
      </div>
    </div>
  );
};

export default Stories;
