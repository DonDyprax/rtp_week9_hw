import Comics from "./Comics";
import { render, screen } from "@testing-library/react";
import { Provider } from "react-redux";
import { Store, persistor } from "../config/Store";
import { PersistGate } from "redux-persist/integration/react";
import { BrowserRouter } from "react-router-dom";

it("Characters List page is rendering", () => {
  render(
    <Provider store={Store}>
      <BrowserRouter>
        <PersistGate persistor={persistor}>
          <Comics />
        </PersistGate>
      </BrowserRouter>
    </Provider>
  );
  expect(screen.getByTestId("comicsList")).toBeTruthy();
});