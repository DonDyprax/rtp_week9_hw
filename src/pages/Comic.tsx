import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { getComic } from "../actions/comicActions";
import _ from "lodash";
import { Link } from "react-router-dom";

const Comic = () => {
  const id = "6181"

  const dispatch = useDispatch();
  const comicState = useSelector((state: any) => state.Comic);

  useEffect(() => {
    dispatch(getComic(6181));
  }, [dispatch, id]);

  const showData = () => {
    if (!_.isEmpty(comicState.data[id])) {
      const comicData = comicState.data[id].data.results[0];
      console.log(comicData);
      return (
        <div data-testid='comicContainer'className="comic-container">
          <h1>{comicState.data[id].data.results[0].title}</h1>
          <img
            src={`${comicData.thumbnail.path}/portrait_uncanny.${comicData.thumbnail.extension}`}
            alt=""
          />
          <p>{comicData.description}</p>

          <div className="comic-characters">
              <h1>Characters that appear in this comic:</h1>
              {comicData.characters.items.map((character: any) => {
                  const path = character.resourceURI.split("/")[6];
                  return (
                    <Link to={`/characters/${path}`}>
                      <p>{character.name}</p>
                    </Link>
                  );
              })}
          </div>

          <div className="comic-stories">
              <h1>Stories that appear in this comic:</h1>
              {comicData.stories.items.map((story: any) => {
                  const path = story.resourceURI.split("/")[6];
                  return (
                    <Link to={`/stories/${path}`}>
                      <p>{story.name}</p>
                    </Link>
                  );
              })}
          </div>
        </div>
      );
    }

    if (comicState.loading) {
      return <h1>Loading...</h1>;
    }

    if (comicState.errorMsg !== "") {
      return <h1>{comicState.errorMsg}</h1>;
    }

    return <h1>Error getting comic</h1>;
  };

  return <div className={"comic"}>{showData()}</div>;
};

export default Comic;
