import Character from "./Character";
import { render, screen, waitFor, waitForElementToBeRemoved } from "@testing-library/react";
import { Provider } from "react-redux";
import { Store, persistor } from "../config/Store";
import { PersistGate } from "redux-persist/integration/react";
import { BrowserRouter } from "react-router-dom";
import user from '@testing-library/user-event';
import { act } from "react-dom/test-utils";


describe("Character Page testing", () => {
    it('Charcter Page loads properly', async () => {
      render(
        <Provider store={Store}>
        <BrowserRouter>
          <PersistGate persistor={persistor}>
            <Character />
          </PersistGate>
        </BrowserRouter>
      </Provider>
      )

      await waitForElementToBeRemoved(() => screen.queryByText(/Loading.../i));
      expect(screen.getByTestId('characterContainer')).toBeInTheDocument();
    screen.debug();
    })
})