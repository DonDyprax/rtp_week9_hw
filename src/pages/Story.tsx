import React, { useEffect } from 'react';
import { useDispatch, useSelector } from "react-redux";
import { getStory } from "../actions/storyActions";
import _ from "lodash";

const Story = () => {
    const id = "659";

    const dispatch = useDispatch();
    const storyState = useSelector((state: any) => state.Story);

    useEffect(() => {
        dispatch(getStory(659));
    }, [dispatch, id]);

    const showData = () => {
        if (!_.isEmpty(storyState.data[id])) {
          const storyData = storyState.data[id].data.results[0];
          
          return (
            <div data-testid='storyContainer' className="story-container">
              <h1>{storyData.title}</h1>
            </div>
          );
        }
    
        if (storyState.loading) {
          return <h1>Loading...</h1>;
        }
    
        if (storyState.errorMsg !== "") {
          return <h1>{storyState.errorMsg}</h1>;
        }
    
        return <h1>Error getting story</h1>;
      };

    return (
        <div className={'story'}>
            {showData()}
        </div>
    )
}

export default Story;